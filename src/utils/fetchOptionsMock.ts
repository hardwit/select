type Option = {
  label: string,
  value: number
}

const OPTIONS = [
  {
    label: 'Аааааа',
    value: 1
  },
  {
    label: 'Ааббббб',
    value: 2
  },
  {
    label: 'Бббббб',
    value: 3
  },
  {
    label: 'ВВвввв',
    value: 4
  },
  {
    label: 'Гггггг',
    value: 5
  },
  {
    label: 'Дддддд',
    value: 6
  },
  {
    label: 'Еееееее',
    value: 7
  },
  {
    label: 'Жжжжжжж',
    value: 8
  },
  {
    label: 'Ззззззз',
    value: 9
  },
  {
    label: 'Ддддддд',
    value: 10
  },
]

export const fetchOptions : (text : string) => Promise<Option[]> = (text) => {
  return new Promise(resolve => {
    let result: Option[] = []

    if (!text) {
      result = OPTIONS.slice(0, 5)
    } else {
      result = OPTIONS.filter(option => option.label.toLowerCase().startsWith(text.toLowerCase()))
    }

    setTimeout(() => {
      resolve(result)
    }, 100)
  })
}