export function debounce<Args extends any[]> (callback :(...args: any[]) => void, ms: number) {
  let timeoutId: ReturnType<typeof setTimeout> | undefined;

  return (...args: Args) => {
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
  
    setTimeout(() => callback(...args), ms)
  }
}