import React, { FC, useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';
import styled from 'styled-components';
import observeRect from '../utils/observeRect';

interface PositionInPortalProps {
  parentRef: React.RefObject<HTMLDivElement>;
  open: boolean;
}

export const PositionInPortal: FC<PositionInPortalProps> = ({ parentRef, children }) => {
  const positionedPortalContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const node = positionedPortalContainerRef.current;

    if (node && parentRef.current) {
      const { x, y, height, width } = parentRef.current.getBoundingClientRect();

      const { style } = node;
      
      style.top = `${ y + height}px`;
      style.left = `${x}px`;
      style.height = `${height}px`;
      style.width = `${width}px`;
    }
  }, [parentRef, positionedPortalContainerRef]);

  useEffect(() => {
    const node = positionedPortalContainerRef.current;

    if (node && parentRef.current) {
      const observer = observeRect(parentRef.current, (rect) => {
        if (rect) {
          const { x, y, height, width } = rect

          const { style } = node;
          style.top = `${ y + height}px`;
          style.left = `${x}px`;
          style.height = `${height}px`;
          style.width = `${width}px`;
        }
      });

      observer.observe();

      return () => {
        observer.unobserve();
      };
    }
  }, [parentRef, positionedPortalContainerRef]);



  return createPortal(
    <PositionedPortalContainer ref={positionedPortalContainerRef}>
      <DropDownContainer>
        {children}
      </DropDownContainer>
    </PositionedPortalContainer>,
    document.body,
  );
};

const PositionedPortalContainer = styled.div`
  position: fixed;
  overflow: visible;
`;

const DropDownContainer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  height: 0;
`;