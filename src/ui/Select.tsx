import React, { FC, useCallback, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { PositionInPortal } from './PositionInPortal';
import { debounce } from '../utils';
import { useOnClickOutside } from '../hooks/useOnclickOutside';

type SelectOptionValue = string | number

export type SelectOption = {
  value: SelectOptionValue,
  label: string,
}

interface SelectProps {
  options?: SelectOption[],
  values: SelectOption[],
  loadOptions: (text: string) => Promise<SelectOption[]>,
  className?: string,
  onSelect: (options: SelectOption[]) => void
}

export const Select: FC<SelectProps> = ({ options = [], loadOptions, values, onSelect, className = '' }) => {
  const [isDropdownOpened, toggleDropdown] = useState<boolean>(false)
  const [inputValue, setInputValue] = useState('')
  const [availableOptions, setAvailableOptions] = useState<SelectOption[]>(options)

  const parentRef = useRef<HTMLDivElement>(null)
  const dropdownRef = useRef<HTMLDivElement>(null)
  const inputRef = useRef<HTMLInputElement>(null)

  const filteredOptions = availableOptions.filter(option => !values.find(o => option.value === o.value))

  const handleSelectClick = () => {
    toggleDropdown(true)

    inputRef.current?.focus()
  }

  const handleOptionClick = (option: SelectOption) => {
    setInputValue('')



    onSelect([...values, option])
  }

  const handleRemoveOptionClick = (option: SelectOption) => {
    const currentOption = values.find(o => o.value === option.value) as SelectOption
    const indexOfOption = values.indexOf(currentOption)

    onSelect([...values.slice(0, indexOfOption), ...values.slice(indexOfOption + 1)])
  }

  const fetchOptions = useCallback(debounce(async (text) => {
    const options = await loadOptions(text)

    setAvailableOptions(options)
  }, 300), [])

  const handleTextInput: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const value = e.target.value

    setInputValue(value)
    fetchOptions(value)
  }

  useEffect(() => {
    fetchOptions()
  }, [fetchOptions])

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        toggleDropdown(false)
        inputRef.current?.blur()
      }
    }

    document.addEventListener('keydown', listener)

    return () => {
      document.removeEventListener('keydown', listener)
    }
  }, [inputRef])

  useOnClickOutside(parentRef, () => {
    toggleDropdown(false)
  }, [dropdownRef])

  return (
    <SelectBox className={className} ref={parentRef} onClick={handleSelectClick}>
      {
        values.map(option => (
          <ChosedOptionBox key={option.value}>
            {option.label}
            <CrossImg src="cross.svg" onClick={() => handleRemoveOptionClick(option)}></CrossImg>
          </ChosedOptionBox>
        ))
      }

      <SelectInput value={inputValue} onChange={handleTextInput} ref={inputRef} />

      {isDropdownOpened &&
        <PositionInPortal parentRef={parentRef} open={isDropdownOpened}>
          <DropdownOptionsBox ref={dropdownRef}>
            {
              filteredOptions.map((option: SelectOption) => (
                <OptionBox onClick={() => handleOptionClick(option)} key={option.value}>
                  {option.label}
                </OptionBox>
              ))
            }

            {!filteredOptions.length && 
              <EmptyTextBox>
                Ничего не найдено
              </EmptyTextBox>
            }
          </DropdownOptionsBox>
        </PositionInPortal>
      }
    </SelectBox>
  )
}

const SelectBox = styled.div`
  width: 100%;
  min-height: 40px;
  height: fit-content;
  border: 1px solid #d4d7d8;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  padding: 4px;
  box-sizing: border-box;
  gap: 4px;
  background: white;
`

const SelectInput = styled.input`
  outline: none;
  height: 60%;
  border: none;
  flex: 1;
`

const ChosedOptionBox = styled.div`
  height: 20px;
  padding: 0 4px;
  background: #e5ebf0;
  color: #5d6d83;
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const DropdownOptionsBox = styled.div`
  width: 100%;
  border: 1px solid #d4d7d8;
  border-top: none;
  box-sizing: border-box;
  background: white;
`

const OptionBox = styled.div`
  width: 100%;
  height: 30px;
  padding-left: 5px;
  display: flex;
  align-items: center;
  color: #5d6d83;
  box-sizing: border-box;

  &:hover {
    background: #e5ebf0;
  }
`

const EmptyTextBox = styled.div`
  width: 100%;
  padding-left: 5px;
  color: #5d6d83;
`

const CrossImg = styled.img`
  margin-left: 5px;
  width: 10px;
  height: 10px;
  cursor: pointer;
  transition: all 0.4s linear;
  
  &:hover {
    filter: brightness(110%);
  }
  
  &:focus, &:active {
    filter: brightness(120%);
  }
`