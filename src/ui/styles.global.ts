import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  html, body {
    width: 100%;
    margin: 0px;
    padding: 0px;
  }

  body {
    font-family: 'Areal', sans-serif;
  }

  p {
    margin: 0;
  }
`