import React, { useState } from 'react';

import styled from 'styled-components';
import { GlobalStyle, Select } from './ui';
import { SelectOption } from './ui/Select';
import { fetchOptions } from './utils';

function App() {
  const [firstSelectValues, setFirstSelectValues] = useState<SelectOption[]>([])
  const [secondSelectValues, setSecondSelectValues] = useState<SelectOption[]>([])

  return (
    <>
    <AppBox>
      <Row>
        <SelectLabel>Первый селект</SelectLabel>

        <StyledSelect loadOptions={fetchOptions} onSelect={setFirstSelectValues} values={firstSelectValues} />
      </Row>
      
      <Row>
        <SelectLabel>Второй селект</SelectLabel>

        <StyledSelect loadOptions={fetchOptions} onSelect={setSecondSelectValues} values={secondSelectValues} />
      </Row>

      <Note>Задал высоту страницы 2000px для демонстрации поведения при скролле</Note>
    </AppBox>

    <GlobalStyle />
    </>
  )
}

const AppBox = styled.div`
  width: 100%;
  height: 2000px;
  display: flex;
  flex-direction: column;
  padding: 20px;
`

const StyledSelect = styled(Select)`
  && {
    width: 300px;
  }
`

const Row = styled.div`
  & ~ & {
    margin-top: 20px;
  }
`

const SelectLabel = styled.p`
  margin-bottom: 5px;
  color: #5d6d83;
`

const Note = styled.div`
  margin-top: 100px;
  width: 100%;
  color: #5d6d83;
  text-align: center;
`

export default App;
